/* test_e3Main.cpp */
/* Author:  Simon Rose */
/* Date:    2021-03-26 */

#include <stddef.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <stdio.h>

#include "epicsExit.h"
#include "epicsThread.h"
#include "iocsh.h"
#include "epicsExport.h"

static const iocshArg helloArg0 = { "name", iocshArgString};
static const iocshArg helloArg1 = { "count", iocshArgInt};

static const iocshArg * const helloArgs[] = { &helloArg0, &helloArg1};

static const iocshFuncDef helloFuncDef = { "hello", 2, helloArgs};

static void hello(const char *name, int count) {
    if (count <= 0) {
        printf("Error: Invalid count\n");
        return;
    }
    while (count-- > 0) {
        printf("Hello %s\n", name);
    }
}

static void helloCallFunc(const iocshArgBuf *args) {
    hello(args[0].sval, args[1].ival);
}

void helloRegister(void) {
    static int firstTime = 1;

    if (firstTime) {
        firstTime = 0;

        iocshRegister(&helloFuncDef, helloCallFunc);
    }
}

epicsExportRegistrar(helloRegister);